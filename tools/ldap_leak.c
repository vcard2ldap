#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ldap.h>

void usage (const char *argv0);
void leak_loop (const char *host, int port, const char *binddn,
  const char *passwd);

int
main (int argc, char **argv)
{
  char *host = "localhost";
  int port = LDAP_PORT;

  char *binddn = NULL;
  char *passwd = NULL;

  int c;

  opterr = 0;

  do
    {
      c = getopt (argc, argv, "h:p:b:x:");

      switch (c)
        {
          case 'h':
            host = optarg;
            break;
          case 'p':
            port = atoi (optarg);
            break;
          case 'b':
            binddn = optarg;
            break;
          case 'x':
            passwd = optarg;
            break;
          case '?':
            usage (argv[0]);
            return 1;
          default:
            break;
        }
    } while (c != -1);

  if (!binddn || !passwd || port <= 0 || port >= 1 << 16)
      usage (argv[0]);

  leak_loop (host, port, binddn, passwd);

  return 0;
}

void
usage (const char *argv0)
{
  fprintf (stderr, "Usage: %s -b binndn -x passwd [-h host] [-p port]\n",
    argv0);
  exit (1);
}

void
leak_loop (const char *host, int port, const char *binddn,
  const char *passwd)
{
  LDAP *ld;
  int version;
  int rc, i;

  for (i = 0; i < 1 << 12; i++)
    {
      ld = ldap_init (host, port);

      version = LDAP_VERSION3;
      ldap_set_option (ld, LDAP_OPT_PROTOCOL_VERSION, &version);

      rc = ldap_simple_bind_s (ld, binddn, passwd);

      if (rc != LDAP_SUCCESS)
        {
          fprintf (stderr, "LDAP Error: %s\n", ldap_err2string (rc));
          exit (1);
        }

      ldap_unbind_s (ld);
#ifdef NOLEAK
      sasl_done ();
#endif
    }
}

