#!/usr/bin/perl -w

#### CONFIG

my $db = "Pg"; # Pg : PostgresSQL, mysql: MySQL...
my $dbuser = "jabber";
my $dbpass = "secret";
my $dbname = "jabberd2";

my $DUMP_QUERY = 0;

##########################
use strict;
use DBI;
use Vcard2Ldap;

my $vcard2db = {
    "FN" => "fn",
    "NICKNAME" => "nickname",
    "URL" => "url",
    "TEL/NUMBER" => "tel",
    "EMAIL/USERID" => "email",
    "TITLE" => "title",
    "ROLE" => "role",
    "BDAY" => "bday",
    "DESC" => "desc",
    "N/FAMILY" => "n-family",
    "N/GIVEN" => "n-given",
    "N/MIDDLE" => "n-middle",
    "N/PREFIX" => "n-prefix",
    "N/SUFFIX" => "n-suffix",
    "ADR/STREET" => "adr-street",
    "ADR/POBOX" => "adr-pobox",
    "ADR/EXTADD" => "adr-extadd",
    "ADR/LOCALITY" => "adr-locality",
    "ADR/REGION" => "adr-region",
    "ADR/PCODE" => "adr-pcode",
    "ADR/CTRY" => "adr-country",
    "ORG/ORGNAME" => "org-orgname",
    "ORG/ORGUNIT" => "org-orgunit",

    "TZ" => "tz",
    "GEO/LAT" => "geo-lat",
    "GEO/LON" => "geo-lon",
    "AGENT/EXTVAL" => "agent-extval",
    "NOTE" => "note",
    "REV" => "rev",
    "SORT-STRING" => "sort-string",

    "KEY/TYPE" => "key-type",
    "KEY/CRED" => "key-cred",

    "PHOTO/TYPE" => "photo-type",
    "PHOTO/BINVAL" => "photo-binval",
    "PHOTO/EXTVAL" => "photo-extval",

    "LOGO/TYPE" => "logo-type",
    "LOGO/BINVAL" => "logo-binval",
    "LOGO/EXTVAL" => "logo-extval",

    "SOUND/PHONETIC" => "sound-phonetic",
    "SOUND/BINVAL" => "sound-binval",
    "SOUND/EXTVAL" => "sound-extval"
};

my $option_populate = 0;
my $option_add_user = 0;
my $option_del_user = 0;
my $option_nuser = "";

my ($ldap, $dbh, $vcard2ldap);

### MAIN
if ($#ARGV < 3) {
    if ($ARGV[0] eq "-p" || $ARGV[0] eq "--populate") {
        $option_populate = 1;
    } elsif ($ARGV[0] eq "-d" || $ARGV[0] eq "--delete") {
        $option_del_user = 1;
    } elsif ($ARGV[0] eq "-u" || $ARGV[0] eq "--update") {
        $option_del_user = 1;
        $option_add_user = 1;
    } elsif ($ARGV[0] =~ /^[\w\-_\.]+@.+/) {
        $option_add_user = 1;
        $option_nuser = $ARGV[0];
    } elsif ($ARGV[0]) {
        usage ();
    }

    if ($option_del_user) {
        if ($ARGV[1] =~ /^[\w\-_\.]+@.+/) {
            $option_nuser = $ARGV[1];
        } else {
            usage ();
        }
    }
} else {
    usage ();
}

$vcard2ldap = Vcard2Ldap::vcard_get_map ();
$ldap = Vcard2Ldap::ldap_open ();
$dbh = db_open ();

set_all_vcards ();
del_user ($option_nuser) if $option_del_user;
add_user ($option_nuser) if $option_add_user;
populate_all_rosters () if $option_populate;

$dbh->disconnect;
exit;

###
sub usage {
    print "usage: jabber2db [-p|--populate] [-d|--delete] [-u|--update] " .
              "[user\@$Vcard2Ldap::jabberd]\n";
    exit (1);
}

sub set_all_vcards {
    my $query = "";

    db_delete_vcards () if $option_populate;

    foreach (Vcard2Ldap::ldap_get_users ($ldap)) {
        $query .= db_set_vcard ($_);
    }

     execute_query ($query);
}

sub add_user {
    my $jid = shift;
    my $user = Vcard2Ldap::jabber_user ($jid);
    my $query = "";

    return if (!$user);

    foreach (Vcard2Ldap::jabber_get_users ()) {
        if ($user->{"jid"} ne $_->{"jid"} &&
            $user->{"rostergroup"} eq $_->{"rostergroup"}) {
            $query .= db_insert_user_to_roster ($user, $_);
            $query .= db_insert_user_to_roster ($_, $user);
        }
    }

    execute_query ($query);
}

sub del_user {
    my $jid = shift;
    my $query = "";

    $query .= "DELETE FROM " . $dbh->quote_identifier ("roster-items") .
        "WHERE " . $dbh->quote_identifier ("collection-owner") .
        "= " . $dbh->quote ($jid) . "OR " . $dbh->quote_identifier ("jid") .
        "= " . $dbh->quote ($jid) . ";\n";

    $query .= "DELETE FROM " . $dbh->quote_identifier ("roster-groups") .
        "WHERE " . $dbh->quote_identifier ("collection-owner") .
        "= " . $dbh->quote ($jid) . "OR " . $dbh->quote_identifier ("jid") .
        "= " . $dbh->quote ($jid) . ";\n";

    $query .= "DELETE FROM " . $dbh->quote_identifier ("vcard") .
        "WHERE " . $dbh->quote_identifier ("collection-owner") .
        "= " . $dbh->quote ($jid) . ";\n";

    execute_query ($query);
}

sub populate_all_rosters {
    my $query = "";

    db_delete_rosters ();

    foreach my $us1 (Vcard2Ldap::jabber_get_users ()) {
        foreach my $us2 (Vcard2Ldap::jabber_get_users ()) {
            if ($us1->{"jid"} ne $us2->{"jid"} &&
                 $us1->{"rostergroup"} eq $us2->{"rostergroup"}) {
                $query .= db_insert_user_to_roster ($us1, $us2) . ";";
            }
        }
    }

    execute_query ($query);
}

sub db_open {
    my $dbh = DBI->connect (
        "DBI:$db:database=$dbname;host=localhost",
        $dbuser,
        $dbpass
    ) or die "jabberd2db: cannot connect to DB!";

    return $dbh;
}

sub execute_query {
    my $query = shift;
    my $rows;

    print $query if ($DUMP_QUERY);

    return $dbh->do ($query) || die "jabber2db: cannot execute query! ($!)";
}

sub db_delete_vcards {
    execute_query ("DELETE FROM " . $dbh->quote_identifier ("vcard") . ";\n");
}

sub db_delete_rosters {
    execute_query ("DELETE FROM " .  $dbh->quote_identifier ("roster-items") .
        ";\nDELETE FROM " . $dbh->quote_identifier ("roster-groups") . ";\n");
}

sub db_insert_vcard {
    my $pers = shift;

    my $squery = "";
    my $ssquery = "";
    my $query = "";

    $query = "INSERT INTO vcard (" .
        $dbh->quote_identifier ("collection-owner");

    while (my ($key, $value) = each (%$vcard2db)) {
        if ($vcard2ldap->{$key} && $pers->get_value ($vcard2ldap->{$key})) {
            $squery   .=  ",". $dbh->quote_identifier ($value);
            $ssquery .= $dbh->quote (
                   @{$pers->get_value ($vcard2ldap->{$key}, asref=>1)}[0]) .
                ",";
        }
    }
    chop ($ssquery);

    $query .= $squery . ") VALUES (" .
        $dbh->quote (Vcard2Ldap::jabber_jid ($pers)) . ", " .
        $ssquery . ");\n";

    return $query;
}

sub db_update_vcard {
    my $pers = shift;

    my $squery = "";
    my $ssquery = "";
    my $query = "";

    $query = "UPDATE vcard SET ";

    while (my ($key, $value) = each (%$vcard2db)) {
        if ($vcard2ldap->{$key} && $pers->get_value ($vcard2ldap->{$key})) {
            $query .=  $dbh->quote_identifier ($value) . " = " .
                $dbh->quote (
                    @{$pers->get_value ($vcard2ldap->{$key}, asref=>1)}[0]) .
                ",";
        }
    }

    chop ($query);

    $query .=  " WHERE ". $dbh->quote_identifier ("collection-owner") . " = " .
         $dbh->quote (Vcard2Ldap::jabber_jid ($pers)) . ";\n";

    return $query;
}

sub db_vcard_exists {
    my $pers = shift;
    my $query;
    my $rc;

    $query = "SELECT COUNT(*) FROM vcard WHERE " .
        $dbh->quote_identifier ("collection-owner") . " = " .
        $dbh->quote (Vcard2Ldap::jabber_jid ($pers));

    return execute_query ($query) > 0;
}

sub db_set_vcard {
    my $pers = shift;
    my $is_update = 0;
    my $query;

    return ""
        if (!Vcard2Ldap::jabber_jid ($pers) ||
            !Vcard2Ldap::jabber_group ($pers));

    $is_update = db_vcard_exists ($pers) if (!$option_populate);

    if ($is_update) {
        db_update_vcard ($pers);
    } else {
        db_insert_vcard ($pers);
    }
}

sub db_insert_user_to_roster {
    my $us1 = shift;
    my $us2 = shift;
    my $query;

    $query = "INSERT INTO " .
        $dbh->quote_identifier ("roster-items") . " (" .
        $dbh->quote_identifier ("collection-owner") .", " .
        $dbh->quote_identifier ("jid") . ", " .
        $dbh->quote_identifier ("to") . ", " .
        $dbh->quote_identifier ("from") . ", " .
        $dbh->quote_identifier ("ask") . ") VALUES (" .
        $dbh->quote ($us1->{"jid"}) . "," .
        $dbh->quote ($us2->{"jid"}) . "," .
        "TRUE, TRUE, 0 );\n";

    $query .= "INSERT INTO " .
        $dbh->quote_identifier ("roster-groups") . " (" .
        $dbh->quote_identifier ("collection-owner") . ",  " .
        $dbh->quote_identifier ("jid") . ", " .
        $dbh->quote_identifier ("group") . ") VALUES (" .
        $dbh->quote ($us1->{"jid"}) . "," .
        $dbh->quote ($us2->{"jid"}) . "," .
        $dbh->quote ($us1->{"rostergroup"}) . ");\n";

    return $query;
}


