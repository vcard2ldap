package Vcard2Ldap;

#### CONFIG

our $jabberd  = "jabber.domain.com";

my $vcardtpl = "../ldap/vcard.xml";

my $basedn = "dc=nodomain";
my $ldapserver = "localhost";
my $disname="cn=admin,dc=nodomain";
my $ldappass= "secret";
my $ldapperson = "irisPerson";
my $ldapchatroom = "";

my $ldapgroup = "ou";
my $ldapgroupmatch = "";
my $ldapjid = "irisUserPresenceID";
my $ldapjidmatch = qr/urn:mace:rediris.es:presence:xmpp:([\w\-_\.]+@.+)/;

##########################

=head1 NAME

Vcard2Ldap - Stuff for convert LDAP entries to Jabberd roster ones.

=head1 SYNOPSIS

    use Vcard2Ldap;
    my $ldap = Vcard2Ldap::ldap_open ();
    my $users = Vcard2Ldap::jabber_get_users ($ldap);
    my $vcard2ldap = Vcard2Ldap::vcard_get_map ();

   foreach ($users) {
        print $_->{"jid"} . "\n";
        print $_->{"rostergroup"} . "\n";
   }

    my $us = $ldap->search (
	base => "dc=nodomain",
	scope => "one",
	filter => "(cn=*)"
     );

     foreach ($us->entries) {
         print Vcard2Ldap::jabber_jid ($_) . "\n";
    }

    $ldap->unbind;

    print $vcard2ldap->{"FN"} . "\n";
    #...

=head1 DESCRIPTION

This module isn't intend for general usage, it was made for the sole purpose of
working together with vcard2ldap jabberd module.

See http://repo.or.cz/w/vcard2ldap.git for details.

=head2 GLOBALS

=over 12

List of global variables.

=item C<$jabberd>

The jabberd server hostname

=item C<$groupname>

The name of the roster group where jids will be stored.

=head2 FUNCTIONS

List of public functions.

=item C<ldap_open>

Open connection. Returns a LDAP handler.

=item C<jabber_get_users>

Returns a list of records: {jid,group}

=item C<jabber_jid>

Returns a jid from an LDAP entry. Empty string if none.

=item C<jabber_group>

Returns the list of groups of the LDAP entry. Empty array if none.

=item C<vcard_get_map>

Returns the "vCard map". This hash maps XML vCard tags to LDAP attributes.

=cut

use strict;
use XML::Simple;
use Net::LDAP;
use Net::LDAP::Control;

my @users;

sub ldap_open {
    my $ldap = Net::LDAP->new ($ldapserver, debug => 0) or die "$@";
    my $mesg = $ldap->bind (
        $disname,
        password => $ldappass,
        version => 3) or die "Cannot bind to ldap server ($!)";

    die $mesg->error if $mesg->code;

    return $ldap;
}

sub ldap_get_users {
    my $ldap = shift;

    my $us = $ldap->search (
                  base => "$basedn",
                  scope => "one",
                  filter => "(&(objectClass=$ldapperson)($ldapgroup=*))"
          );
    die $us->error if $us->code;

    if ($#users == -1) {
        add_users ($us);
        add_chatrooms ($ldap) if (length ($ldapchatroom) > 0);
    }

    return $us->entries;
}

sub jabber_get_users {
    my $ldap = shift;

    ldap_get_users ($ldap) if ($#users == -1);

    return @users;
}

sub jabber_user {
    my $ujid = shift;

    foreach (Vcard2Ldap::jabber_get_users ()) {
         return $_ if ($_->{"jid"} eq $ujid);
    }

   warn $ujid . ": user not in directory or malformed entry!";
   return "";
}

sub jabber_jid {
    my $user = shift;
    my $attr = $user->get_value ($ldapjid, asref => 1);
    my $jid = "";

    if ($attr) {
        foreach (@{$attr}) {
            if (length ($ldapjidmatch) == 0) {
                $jid = lc ($_);
            } elsif ($_ =~ $ldapjidmatch) {
                $jid = lc ($1);
            }

            if ($jid) {
                if (index ($jid, "@") == -1) {
                    $jid .= "\@$jabberd";
                }
                last;
            }
        }
    }

    warn $user->get_value ("cn") . ": user with no jid!" if (!$jid);
    return $jid;
}

sub jabber_group {
    my $user = shift;
    my $attr = $user->get_value ($ldapgroup, asref => 1);
    my @group = ();

    if ($attr) {
        foreach (@{$attr}) {
            my $tmp;

            if (length ($ldapgroupmatch) == 0) {
                $tmp = lc ($_);
            } elsif ($_ =~ $ldapgroupmatch) {
                $tmp = lc ($1);
            }

            (push @group, $tmp) if ($tmp);
        }
    }

    if ($#group == -1) {
        warn $user->get_value ("cn") . ": group is mandatory!";
    }

    return @group;
}

sub vcard_get_map {
    my $xs = new XML::Simple (keeproot => 0, suppressempty => 1);
    my $vcard = $xs->XMLin ($vcardtpl);
    my $vcard2ldap;

    while (my ($key, $value) = each (%$vcard)) {
        for (split (/\n/, parse_vcard ($key, $value))) {
            my @sp = split (/ /, $_);
            $vcard2ldap->{$sp[0]} = $sp[1];
        }
    }

    return $vcard2ldap;
}

### private

sub add_users {
    my $us = shift;

    foreach ($us->entries) {
        my $jid = jabber_jid ($_);
        my @rgroup = jabber_group ($_);

        if ($jid) {
            foreach (@rgroup) {
                push @users , {
                      "jid" => $jid,
                      "rostergroup" => $_
                }
            }
        }
    }
}

sub add_chatrooms {
    my $ldap = shift;

    my $us = $ldap->search (
                  base => "$basedn",
                  scope => "one",
                  filter => "(&(objectClass=$ldapchatroom)($ldapgroup=*))"
          );

    die $us->error if $us->code;

    add_users ($us);
}

sub parse_vcard {
    my $key = shift;
    my $value = shift;
    my $ret = "";

    if (!ref ($value) && !($key eq "xmlns")) {
        $ret .= $key . " " . $value . "\n";
    } elsif (ref ($value) eq "HASH" ) {
        for (keys %$value) {
            if ($_ eq "v2ln") {
                next;
            } elsif ($_ eq "content") {
                $ret .= "$key". parse_vcard ("", $value->{$_});
            } else {
                $ret .= "$key/". parse_vcard ($_, $value->{$_});
            }
        }
    } elsif (ref ($value) eq "ARRAY") {
        for (@$value) {
            $ret .= "$key" . parse_vcard ("", $_);
        }
    }

    return $ret;
}

1;
