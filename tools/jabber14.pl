#!/usr/bin/perl -w

use strict;
use Vcard2Ldap;

my $ldap;

### MAIN
my $file = ">-";
my $group;

if ($#ARGV == -1 ||  $#ARGV > 1 || $ARGV[0] eq "-h" || $ARGV[0] eq "--help") {
    usage ();
}

$group = $ARGV[0];

if ($#ARGV == 1) {
	$file = $ARGV[1];
}

open (OUT, ">$file") or die "jabber14: cannot open file! \"$file\" ($!)";

$ldap = Vcard2Ldap::ldap_open ();

print OUT "<query xmlns='jabber:iq:roster' xdbns='jabber:iq:roster'>\n";

foreach (Vcard2Ldap::jabber_get_users ($ldap)) {
     print OUT "\t<item jid='" . $_->{"jid"} . "' subscription ='both'>" .
                         "<group>" . $_->{"rostergroup"} . "</group>" .
                         "</item>\n" if ($_->{"rostergroup"} eq $group);
}

print OUT "</query>\n";
close (OUT);

exit;

###
sub usage {
    print "usage: jabber14 [-h|--help] rostergroup [roster.xml]\n";
    exit (1);
}
