/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_config.c
    \brief Config handling functions. Implementation
*/

#include <stdlib.h>
#include <strings.h>

#ifndef _V2L_JABBER2
#include <jabberd.h>
#else
#include "../util/util.h"

#define log_warn log_debug
#define log_error log_debug
typedef pool_t pool;
#endif

#include <v2l_config.h>
#include <v2l_conn.h>

/*! \brief Gets value for tag from config
    Generic (preprocessor) function for handling Jabberd14/2 config differences
*/
static char *_v2l_config_get_tag (T_CONF conn_base, const char *tag);

/* public api */

int
v2l_config_init (v2l_Config *self, T_CONF cfgroot)
{
  T_CONF conn_base;
  const char *portstr;

  if (!cfgroot)
    {
      log_error (ZONE, "xdb_v2l configuration not present");
      return 0;
    }

#ifndef _V2L_JABBER2
  /* Have a look in the XML configuration data for connection information. */
  conn_base = xmlnode_get_tag (cfgroot, "connection");

  if (conn_base == NULL)
    {
      log_error (ZONE,"<connection> tag is not present");
      return 0;
    }
#else
  conn_base = cfgroot;
#endif

  self->host = _v2l_config_get_tag (conn_base, "host");

  if  (self->host == NULL)
    {
      log_error (ZONE, "LDAP server is not specified");
      return 0;
    }

  portstr = _v2l_config_get_tag (conn_base, "port");
  self->port = portstr != NULL ? atoi (portstr) : LDAP_PORT;

  self->suffix = _v2l_config_get_tag (conn_base, "suffix");

  if (self->suffix == NULL)
    {
      log_error (ZONE, "LDAP suffix is not specified");
      return 0;
    }

  self->filter = _v2l_config_get_tag (conn_base, "filter");

  if (self->filter == NULL)
    {
      log_error (ZONE, "LDAP search filter is not specified");
      return 0;
    }

  self->binddn = _v2l_config_get_tag (conn_base, "binddn");

  if (self->binddn == NULL)
    {
      log_error (ZONE, "LDAP jabber admin dn is not specified");
      return 0;
    }

  self->bindpw = _v2l_config_get_tag (conn_base, "bindpw");

  if (self->bindpw == NULL)
    {
      log_error (ZONE,
          "LDAP jabber admin password is not specified");
       return 0;
    }

  self->confpath = _v2l_config_get_tag (conn_base, "tpl");

  if (self->confpath == NULL)
    {
      log_error (ZONE,
          "Path to vCard <->LDAP is not specified");
       return 0;
    }

  self->irishack = _v2l_config_get_tag (conn_base, "irishack") != NULL;

  self->poolref = pool_new ();

  if (self->poolref == NULL)
    {
      log_error (ZONE, "Cannot create pool");
      return 0;
    }

  /* Get a handle to an LDAP connection. */
  log_debug (ZONE, "LDAP server: %s / port : %d", self->host, self->port);

  self->master_conn = v2l_get_master_conn (self);

  if (self->master_conn == NULL)
    {
      log_error (ZONE,
        "Unable to create the LDAP main connection");
      return 0;
    }

  return 1;
}

void
v2l_config_shutdown (v2l_Config *self)
{
  if (self != NULL && self->master_conn != NULL)
    {
      v2l_free_allconn ();
      ldap_unbind_s (self->master_conn->ld);
      pool_free (self->master_conn->poolref);
      pool_free (self->poolref);
    }
}

/* public api ends here */

static char *
_v2l_config_get_tag (T_CONF conn_base, const char *tag)
{
#ifndef _V2L_JABBER2
  xmlnode tmp;

  tmp = xmlnode_get_tag (conn_base, tag);

  if (tmp == NULL)
    {
      return NULL;
    }

  return (char *) xmlnode_get_data (xmlnode_get_firstchild (tmp));
#else
  char str[32] = "v2l.";

  return config_get_one (conn_base, strncat (str, tag, 27), 0);
#endif
}
