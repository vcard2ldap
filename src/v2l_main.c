/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_main.c
    \brief Module skeleton.
    \mainpage
      \dot
        digraph Packages {
          node [shape = plaintext, fontname = Helvetica, fontsize = 12]
          server [shape = egg, label = jabberd ]
          vcard [
            label =<<table href="v2l__vcard_8h.html">
              <tr><td bgcolor="gray">v2l_vcard</td></tr>
              <tr><td>XML vCard Handling</td></tr>
            </table>>
          ]
          config [
            label =<<table href="v2l__config_8h.html">
              <tr><td bgcolor="gray">v2l_config</td></tr>
              <tr><td>Config handling package</td></tr>
            </table>>
          ]
          conn [
            label =<<table href="v2l__conn_8h.html">
              <tr><td bgcolor="gray">v2l_conn</td></tr>
              <tr><td>LDAP-level package</td></tr>
            </table>>
          ]
          config -> server
          vcard -> config
          vcard -> conn
          conn -> config
          server -> vcard
        }
      \enddot
*/

#include <stdlib.h>
#include <jabberd.h>
#include <v2l_config.h>
#include <v2l_conn.h>
#include <v2l_vcard.h>

#define LOG_ERROR_MEM log_error (ZONE, "Unable to allocate memory")

static result _v2l_packets_handler (instance i, dpacket p, void *args);
static int _v2l_check_attr_value (xmlnode node, char *attr_name,
  char *attr_value);
static void _v2l_shutdown (void *arg);

/*! \brief Module entry point.
    See jabberd14 API docs for details.
*/
#ifdef __cplusplus
extern "C"
#endif
void
xdb_v2l (instance i, xmlnode x)
{
  xdbcache xc = NULL;    /* for config request */
  v2l_Config *self = NULL;

  log_debug (ZONE, "Loading xdb_v2l");

  self = (v2l_Config *) pmalloco (i->p, sizeof (*self));

  if (self == NULL)
    {
      return;
    }

  self->poolref = i->p;

  /* Load config from xdb */
  xc = xdb_cache (i);
  self->config = xdb_get (xc, jid_new (xmlnode_pool (x), "config@-internal"),
    "jabberd:xdb_v2l:config");

  /* Parse config */
  if (!v2l_config_init (self, self->config))
    {
      /* do no try to go on with a badly configured ldap component */
      exit (1);
    }

  register_phandler (i, o_DELIVER, _v2l_packets_handler, (void *) self);
  register_shutdown (_v2l_shutdown, (void *) self);

  log_debug (ZONE, "xdb_v2l has been successfully initialized");
}

/*! \brief shutdown and cleanup callback
    See jabberd14 API docs for details.
*/
static void
_v2l_shutdown (void *arg)
{
  v2l_config_shutdown ((v2l_Config *) arg);
}

/*! \brief Main callback. Handle xdb packets
    See jabberd14 API docs for details.
*/
static result
_v2l_packets_handler (instance i, dpacket p, void *args)
{
  v2l_Config *self = (v2l_Config *) args;
  v2l_LdapConn *user_conn;
  dpacket dp;

  if ((p == NULL) || (p->x == NULL) || (p->id == NULL))
    {
      log_error (ZONE, "malformed xdb packet");
      return r_ERR;
    }

  if (_v2l_check_attr_value (p->x, "type", "error"))
    {
      xmlnode_free (p->x);
      log_warn (ZONE, "xdb_v2l received an error packet");
      return r_DONE;
    }

  if (!_v2l_check_attr_value (p->x, "ns", NS_VCARD))
    {
      log_warn (ZONE,
        "xdb_v2l received a packet for other namespace than NS_VCARD");
      return r_PASS;
    }

  user_conn = v2l_get_conn (self, p->id->user);

  if (user_conn == NULL)
    {
      log_error (ZONE, "Unable to create connection for \"%s\" user",
        p->id->user);
      return r_ERR;
    }

  if (_v2l_check_attr_value (p->x, "type", "set"))
    {
      xmlnode child = xmlnode_get_firstchild (p->x);

      if (v2l_vcard_set (self, user_conn, child) != 1)
        {
          return r_ERR;
        }
    }
  else
    {
      xmlnode data = v2l_vcard_get (self, user_conn);

      if (data)
        {
          xmlnode_insert_tag_node (p->x, data);
          xmlnode_free (data);
        }
    }

  /* making XML reply */
  xmlnode_put_attrib (p->x, "type", "result");
  xmlnode_put_attrib (p->x, "to", xmlnode_get_attrib (p->x, "from"));
  xmlnode_put_attrib (p->x, "from", jid_full (p->id));

  dp = dpacket_new (p->x);

  if (dp == NULL)
    {
      LOG_ERROR_MEM;
      return r_ERR;
    }

  deliver (dp, NULL);

  return r_DONE;
}

static int
_v2l_check_attr_value (xmlnode node, char *attr_name, char *value)
{
  if ((node == NULL) || (attr_name == NULL) || (value == NULL))
    {
      log_debug (ZONE, "_v2l_check_attr_value () parameters are not valid");
      return 0;
    }

  return strncmp (xmlnode_get_attrib (node, attr_name), value,
      strlen (value)) == 0;
}
