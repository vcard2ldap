/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_vcard.c
    \brief XML vCard's to/from LDAP objects translation functions.
    Implementation
*/

#include <stdlib.h>

#ifndef _V2L_JABBER2
#include <jabberd.h>
#else
#include "../util/util.h"
#include <xmlnode.h>

#define log_warn log_debug
#define log_error log_debug

#endif
#include <v2l_vcard.h>

#define LOG_ERROR_MEM log_error (ZONE, "Unable to allocate memory")

/*! \brief vCard template item.
    SLL. All of relations tag<->LDAP attr read from template
*/
typedef struct v2l_vCardItem
{
  char *vcard;  /*!< tag name */
  char *ldap;   /*!< LDAP attribute */
  char *group;  /*!< XML parent of the tag */
  struct v2l_vCardItem *next;
} v2l_vCardItem;

/*! \brief Gets the translation map.
    If it is not initilized yet, the function reads the template from disk
    and sets the map.
    \param self Module config, if map is initialized can be NULL.
    \return The map, NULL if error.
*/
static v2l_vCardItem *_v2l_vcard_map (v2l_Config *self);

/*! \brief Creates the FN tag
    Creates FN tag from GIVEN and FAMILY tags.
    \param vcard The vCard.
    \return 1 if no error, otherwise 0.
*/
static int _v2l_create_fn (xmlnode vcard);

/*! \brief Sets sn1 and sn2 from sn
    Hack. IrisPerson has two surnames: first surname and mother's maiden name.
    This function splits sn in two. Should be called when the list of request
    are complete and before program excutes them.
    \param req The SLL of LDAP requests.
*/
static v2l_LdapRequest *_v2l_set_sn12 (v2l_LdapRequest *req);

/*! \brief Map function from LDAP to XML vCard
    \param item Info about the tag (name, parent group and attribute associated)
    \param vals array of values for the attribe.
    \param[in,out] res The vCard, output is appended here.
    \return res + new tag or res if error.
*/
static xmlnode _v2l_ldap2vcard_generic (v2l_vCardItem *item, const char **vals,
  xmlnode res);

/*! \brief Map function from XML Vcard to LDAP
    \param item Info about the tag (name, parent group and attribute associated)
    \param data Thhe vCard.
    \param[in,out] req List of requests, the new request is appended here.
    \return req + new request or req if error.
*/
static v2l_LdapRequest *_v2l_vcard2ldap_generic (v2l_vCardItem *item,
  xmlnode data, v2l_LdapRequest *req);

/*! \brief Finds tag linked to attr
    Parses the translation map since 'item' node.
    \param item Where the search starts.
    \param attr The attribute;
    \return node with tag<->attr association or NULL if error or not found.
*/
static v2l_vCardItem *_v2l_vcard_find_attr (v2l_vCardItem *item,
  const char *attr);

/*! \brief Is LDAP object attribute linked to any tag?
    Utility function.
    \param attr The attribute.
    \param[out] shrdata Contents the address of any map's node. NULL if no match
    \return true if attr is linked to any tag, otherwise false
*/
static int _v2l_vcard_attr_match (const char *attr, void **shrdata);

/*! \brief Function applied to all attrs.
    Utility function, walker function.
    \param attr The attribute.
    \param vals Values of attribute.
    \param pointer Deferenced pointer to vCard.
    \param shrdata Data shared with the match function.
*/
static void _v2l_vcard_attr_value (const char *attr, const char **vals,
  void *pointer, void *shrdata);

#if 0
static v2l_vCardItem *
  _v2l_vcard_find_tag (v2l_vCardItem *item, char *tag);
#endif

/*! List of all XML vCard tags supported */
static const char *_V2L_MAP_VCARD [] = {
    "FN",
    "NICKNAME",
    "URL",
    "TEL/NUMBER",
    "EMAIL/USERID",
    "TITLE",
    "ROLE",
    "BDAY",
    "DESC",
    "N/FAMILY",
    "N/GIVEN",
    "N/MIDDLE",
    "N/PREFIX",
    "N/SUFFIX",
    "ADR/STREET",
    "ADR/POBOX",
    "ADR/EXTADD",
    "ADR/LOCALITY",
    "ADR/REGION",
    "ADR/PCODE",
    "ADR/CTRY",
    "ORG/ORGNAME",
    "ORG/ORGUNIT",

    "TZ",
    "GEO/LAT",
    "GEO/LON",
    "AGENT/EXTVAL",
    "NOTE",
    "REV",
    "SORT-STRING",

    "KEY/TYPE",
    "KEY/CRED",

    "PHOTO/TYPE",
    "PHOTO/BINVAL",
    "PHOTO/EXTVAL",

    "LOGO/TYPE",
    "LOGO/BINVAL",
    "LOGO/EXTVAL",

    "SOUND/PHONETIC",
    "SOUND/BINVAL",
    "SOUND/EXTVAL",

    NULL
};

/* public api */

xmlnode
v2l_vcard_get (v2l_Config *self, v2l_LdapConn *curr_conn)
{
  xmlnode vcard;
  v2l_LdapEvt *evt_res;

  if (_v2l_vcard_map (self) == NULL)
    {
      log_error (ZONE, "Unreadable/malformed vCard template!");
      return NULL;
    }

  /* get user info from LDAP */
  evt_res = v2l_ldap_get_entry (self, curr_conn);

  if (evt_res == NULL)
    {
      return NULL;
    }

  /* prepare the XML result */
  vcard = xmlnode_new_tag ("vCard");
  xmlnode_put_attrib (vcard, "xmlns", "vcard-temp");

  v2l_ldap_for_all_attrs (_v2l_vcard_attr_value, _v2l_vcard_attr_match, vcard,
    evt_res);
  free (evt_res);

  if (self->irishack == 1)
    {
      xmlnode photo;

      /* <FN/> := <GIVEN/> + <FAMILY/> */

      _v2l_create_fn (vcard);

      /* InetOrgPerson has a jpegphoto attribute so PHOTO/MIMETYPE tag can be
         FIXME: RedIRIS schema doesn't support avatars in another format (png)
      */

      photo = xmlnode_get_tag (vcard, "PHOTO");

      if (photo != NULL)
        {
          /* FIXME: mimetype is hardcoded */
          xmlnode mimetype = xmlnode_insert_tag (photo, "TYPE");
          xmlnode_insert_cdata (mimetype, "image/jpeg", sizeof ("image/jpeg"));
        }
    }

  return vcard;
}

int
v2l_vcard_set (v2l_Config *self, v2l_LdapConn *curr_conn, xmlnode data)
{
  xmlnode node;
  v2l_LdapRequest *ldap_req = NULL;
  v2l_vCardItem *item;

  if (data == NULL)
    {
      log_warn (ZONE, "vCard data is NULL?");
      return 0;
    }

  item = _v2l_vcard_map (self);

  if (item == NULL)
    {
      log_error (ZONE, "Unreadable/Malformed vCard template!");
      return 0;
    }

  do
    {
      if (self->irishack == 1 && strcmp (item->vcard, "FN") == 0)
        {
          goto is_fn;
        }

      if (item->group != NULL)
        {
          char tag[30];

          sprintf (tag, "%s/%s", item->group, item->vcard);
          node = xmlnode_get_tag (data, tag);
        }
      else
        {
          node = xmlnode_get_tag (data, item->vcard);
        }

      if (node != NULL)
        {
          ldap_req = _v2l_vcard2ldap_generic (item, node, ldap_req);
        }
is_fn:
      item = item->next;
    } while (item != NULL);

  if (self->irishack == 1)
    {
      ldap_req = _v2l_set_sn12 (ldap_req);
    }

  return v2l_request_record (self, curr_conn, ldap_req);
}

/* public api ends here */

static v2l_vCardItem *
_v2l_vcard_map (v2l_Config *self)
{
  static v2l_vCardItem *_V2L_TPL = NULL;
  xmlnode tpl, tag;
  char **stag, *tmp, group[10];
  v2l_vCardItem *item;

  if (_V2L_TPL == NULL && self != NULL && self->confpath != NULL)
    {
      tpl = xmlnode_file (self->confpath);

      if (tpl == NULL)
        {
          return NULL;
        }

      for (stag = (char **) _V2L_MAP_VCARD; *stag != NULL; stag++)
        {
          tmp = strchr (*stag, '/');

          if (tmp == NULL)
            {
              group[0] = 0;
              tmp = *stag;
            }
          else
            {
              sprintf (group, "%.*s", tmp - *stag, *stag);
              tmp++;
            }

          tag = xmlnode_get_tag (tpl, *stag);

          if (xmlnode_get_data (tag) != NULL)
            {
              int ntags = 0;
              char find_attr[30];

              do
                {
                  v2l_vCardItem *ptr;

                  ptr = (v2l_vCardItem *) pmalloc (self->poolref,
                    sizeof (v2l_vCardItem));

                  ptr->vcard = tmp;
                  ptr->ldap  = pstrdup (self->poolref, xmlnode_get_data (tag));
                  ptr->next  = NULL;
                  ptr->group = group[0] == 0 ? NULL :
                  pstrdup (self->poolref, group);

                  if (_V2L_TPL == NULL)
                    {
                      _V2L_TPL = ptr;
                      item = ptr;
                    }
                  else
                    {
                      item->next = ptr;
                      item = item->next;
                    }

                  sprintf (find_attr, "%s?v2ln=%d", *stag, ++ntags);
                  tag = xmlnode_get_tag (tpl, find_attr);
                } while (tag && xmlnode_get_data (tag) != NULL && ntags < 10);
            }/* xmlnode_get_data (tag) != NULL */
        }  /* for loop, all tags in template */

      xmlnode_free (tpl);
    }

  return _V2L_TPL;
}

#if 0
static v2l_vCardItem *
_v2l_vcard_find_tag (v2l_vCardItem *item, char *tag)
{
  v2l_vCardItem *res = NULL;

  while (item != NULL)
    {
      if (strcmp (item->vcard, tag) == 0)
        {
          res = item;
          break;
        }

        item = item->next;
    }

  return res;
}
#endif

static v2l_vCardItem *
_v2l_vcard_find_attr (v2l_vCardItem *item, const char *attr)
{
  v2l_vCardItem *res = NULL;

  while (item != NULL)
    {
      if (strcmp (item->ldap, attr) == 0)
        {
          res = item;
          break;
        }

        item = item->next;
    }

  return res;
}

static int
_v2l_vcard_attr_match (const char *attr, void **shrdata)
{
  *shrdata = _v2l_vcard_find_attr (_v2l_vcard_map (NULL), attr);

  return *shrdata != NULL;
}

static void
_v2l_vcard_attr_value (const char *attr, const char **vals, void *pointer,
  void *shrdata)
{
  xmlnode vcard;
  v2l_vCardItem *match;

  vcard = (xmlnode) pointer;
  match = (v2l_vCardItem *) shrdata;

  if (vals != NULL)
    {
        _v2l_ldap2vcard_generic (match, vals, vcard);
    }
}

static int
_v2l_create_fn (xmlnode vcard)
{
  xmlnode n, fn;
  char *family, *given, *fn_str;
  int len;

  fn = xmlnode_insert_tag (vcard, "FN");
  n  = xmlnode_get_tag (vcard, "N");
  family = xmlnode_get_tag_data (n, "FAMILY");
  given  = xmlnode_get_tag_data (n, "GIVEN");

  len = 0;
  len += (family != NULL) ? strlen (family) : 0;
  len += (given  != NULL) ? strlen (given)  : 0;

  if (len == 0)
    {
      log_debug (ZONE, "<fn><n>...</n></fn> is empty, returning");
      return 1;
    }

  fn_str = (char *) malloc (sizeof (char) * (len + 2));

  if (fn_str == NULL)
    {
      LOG_ERROR_MEM;
      return 0;
    }

  fn_str[0] = 0;

  if (family != NULL)
    {
      sprintf (fn_str, "%s ", family);
    }

  if (given != NULL)
    {
      strcat (fn_str, given);
    }

  xmlnode_insert_cdata (fn, fn_str, len + 1);
  free (fn_str);

  return 1;
}

/* LDAP -> vCard */

static xmlnode
_v2l_ldap2vcard_generic (v2l_vCardItem *item, const char **vals, xmlnode res)
{
  xmlnode node;

  if (item->group != NULL)
    {
      node = xmlnode_get_tag (res, item->group);

      if (node == NULL)
        {
          node = xmlnode_insert_tag (res, item->group);
        }
    }
  else
    {
      node = res;
    }

  node = xmlnode_insert_tag (node, item->vcard);
  xmlnode_insert_cdata (node, vals[0], strlen (vals[0]));

  return res;
}

/*  vCard -> LDAP */

static v2l_LdapRequest *
_v2l_vcard2ldap_generic (v2l_vCardItem *item, xmlnode data,
  v2l_LdapRequest *req)
{
  const char *str;

  str = xmlnode_get_data (data);

  return str == NULL ? req : v2l_add_attr_str (req, item->ldap, str);
}

static v2l_LdapRequest *
_v2l_set_sn12 (v2l_LdapRequest *req)
{
  if (req != NULL)
    {
      v2l_LdapRequest *ptr;
      char *sn;

      for (ptr = req, sn = NULL; ptr != NULL; ptr = ptr->next)
        {
          if (strcmp (ptr->attr->mod_type, "sn") == 0)
            {
              sn = ptr->attr->mod_values[0];
              break;
            }
        }

      if (sn != NULL)
        {
          char *sit, *sn1, *sn2;

          for (sit = sn; *sit != ' ' && *sit != 0; sit++);

          sn1 = (char *) malloc (sizeof (char) * (sit - sn + 1));

          if (sn1 == NULL)
            {
              return req;
            }

          strncpy (sn1, sn, sizeof (char) * (sit - sn));
          sn1[sit - sn] = 0;
          ptr = v2l_add_attr_str (req, "sn1", sn1);
          free (sn1);

          if (ptr == NULL)
            {
              return req;
            }

          req = ptr;

          if (sit - sn != strlen (sn))
            {
              sn2 = (char *) malloc (strlen (sit) + 1);

              if (sn2 == NULL)
                {
                  LOG_ERROR_MEM;
                  return req;
                }

              strcpy (sn2, sit);
              ptr = v2l_add_attr_str (req, "sn2", sn2);
              free (sn2);

              if (ptr == NULL)
                {
                  return req;
                }

              req = ptr;
            }
        }  /* sn != NULL */
    }  /* req != NULL */

  return req;
}
