/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_conn.c
    \brief Handling the LDAP directory. Implementation
*/

#include <stdlib.h>

#ifndef _V2L_JABBER2
#include <jabberd.h>
#else
#include <pthread.h>
#include "../util/util.h"

#define log_warn log_debug
#define log_error log_debug
typedef pool_t pool;
#endif

#include <v2l_conn.h>

/*! \brief Lifetime of an ephemeral connection, in seconds
    The recall connections thread period.
*/
#define V2L_CONN_LIFETIME 30

/*! How often thread checks for LDAP results, in seconds */
#define V2L_POLL_INTERVAL 1

#define LOG_ERROR_MEM log_error (ZONE, "Unable to allocate memory")

/*! Global hashtable of all currently active LDAP connections */
static xht V2L_CONN_LIST = NULL;

/*! \brief Creates new LDAP connection
    \param host LDAP hostname.
    \param port LDAP port.
    \param binddn Bind DN.
    \param user User name.
    \param passwd User password.
    \param expire Boolean, ephemeral connection?
    \return LDAP connectior or NULL if error.
*/
static v2l_LdapConn *_v2l_create_conn (char *host, int port, const char *binddn,
  const char *user, const char *passwd, int expire);

/*! \brief Gets user credentials
    Retrieves user password and common name from LDAP directory using master
    connection.
    \note Allocated memory (*passwd and *cn) must be freed by the caller.
    \param self Module config.
    \param user User name.
    \param[out] passwd User password.
    \param[out] cn Common Name.
    \pre self is valid, user is not NULL
    \return 1 if no error, otherwise 0.
*/
static int _v2l_ldap_get_credentials (v2l_Config *self, const char *user,
  char **passwd, char **cn);

/*! \brief Closes and frees a connection.
    Utility function, common code for two hash walker functions.
    \param h Jabberd hash. Hash table "username"->connection.
    \param user User name.
    \param val deference pointer to connection.
    \sa _v2l_free_walker
    \sa _v2l_free_expired_walker
*/
static void _v2l_free_conn (xht h, const char *user, void *val);

/*! \brief Gets the number or attributes in a LDAP request.
    \param[in,out] req The SLL of LDAP requests.
    \return number of attributes, 0 if req is NULL.
*/
static int _v2l_count_attrs (v2l_LdapRequest *req);

/*! \brief Modifies an entry in the LDAP directory
    Utility function.
    \param dn Entry bind DN
    \param attrs List of attributes changed.
    \param[in,out] evt_res Control and info parameter.
    \return LDAP error code.
*/
static int _v2l_ldap_modify (char *dn, LDAPMod **attrs, v2l_LdapEvt *evt_res);

/*! \brief Searchs and retrieves an entry from the LDAP directory
    Utility function.
    \param dn Entry bind DN.
    \param suffix LDAP suffix
    \param[out] attrs Entry attributes.
    \param subtree boolean. Search in a one level or in entire subtree?
    \param[in,out] evt_res Control and info parameter.
    \return LDAP error code.
*/
static int _v2l_ldap_search (char *dn, char *suffix, char **attrs, int subtree,
  v2l_LdapEvt *evt_res);

/*! \brief Builds and returns the search filter
    \note Allocated memory must be freed by the caller.
    \param self Module config.
    \param user User name.
    \sa _v2l_ldap_search
    \return The filter or NULL if error.
*/
static char *_v2l_ldap_filter (v2l_Config *self, const char *user);

/*! \brief Waits for LDAP results.
    Operations on the directory are asynchronous. This is a sync wait function.
    \param[in,out] evt_res Control and info parameter.
*/
static void _v2l_ldap_sync (v2l_LdapEvt *evt_res);

/*! \brief Adds LDAP attr to request.
    Utility function, hides OpenLDAP API to upper level.
    \pre Attribute is not NULL.
    \param[in,out] req The SLL of LDAP requests. If NULL new list is returned.
    \param attr The attr, LDAPMod pointer.
    \return The list of requests + the last added. The list of request if error.
*/
static v2l_LdapRequest *_v2l_add_attr (v2l_LdapRequest *req, LDAPMod *attr);

/*! \brief Adds a conn to the global list.
    If list is empty, initializes it and starts the thread who recalls expired
    connections.
    \param ldap_conn New connection.
*/
static void _v2l_add_conn (v2l_LdapConn *ldap_conn);

/*! \brief Frees connection unconditionally.
    Utility function. Hash walker function.
    See jabberd API for details.
*/
static void _v2l_free_walker (xht h, const char *key, void *val,
  void *arg);

/*! \brief Frees connections when its slice time expired (periodic thread)
  \dot
  digraph G {
    rankdir = LR
    node [shape = ellipse, fontname = Helvetica fontsize = 12]
    {node[style = filled, fontsize = 16, bgcolor = grey] sleep}
    {node[shape = circle, label = delete] free}
    {node[label = "expired?" ] is_expired}
    {node[label = "conn left?" ] any_conn}
    sleep -> sleep
    sleep -> any_conn [label = "wake!"]
    any_conn -> sleep [label = no]
    any_conn -> is_expired [label = yes]
    is_expired -> free [label = yes]
    is_expired -> any_conn [label = no]
    free -> any_conn
  }
  \enddot
  \param arg Unused.
*/
static void *_v2l_purge_conn_callback (void *arg);

/*! \brief Frees expired connections.
    Utility function. Hash walker function.
    See jabberd API for details.
*/
static void _v2l_free_expired_walker (xht h, const char *key, void *val,
  void *arg);

/*! A thread for active wait for LDAP results */
static int _v2l_ldap_wait_callback (void *arg);

#ifdef _V2L_JABBER2
static void *_v2l_ldap_wait_callback_g (void *arg);
#endif

/* public api */

v2l_LdapConn *
v2l_get_conn (v2l_Config *self, const char *user)
{
  v2l_LdapConn *user_conn;

  user_conn = (v2l_LdapConn *) xhash_get (V2L_CONN_LIST, user);

  if (user_conn == NULL)
    {
      char *passwd, *cn, *binddn;

      /* Get the user password for connecting him to LDAP server */
      if (_v2l_ldap_get_credentials (self, user, &passwd, &cn) == 0)
        {
          log_error (ZONE, "User \"%s\" not found in the directory", user);
          return NULL;
        }

      binddn = (char *) malloc (strlen (self->suffix) + strlen (cn) + 4);

      if (binddn == NULL)
        {
          LOG_ERROR_MEM;
          free (passwd);
          free (cn);
          return NULL;
        }

      sprintf (binddn, "cn=%s,%s", cn, self->suffix);

      log_debug (ZONE, "Attempting to connect with DN: %s", binddn);

      user_conn = _v2l_create_conn (self->host, self->port, binddn, user,
        passwd, 1);

      free (passwd);
      free (cn);
      free (binddn);

    } /* user_conn == NULL */

  return user_conn;
}

v2l_LdapConn *
v2l_get_master_conn (v2l_Config *self)
{
  return _v2l_create_conn (self->host, self->port, self->binddn, V2L_ADMIN,
    self->bindpw, 0);
}

void
v2l_free_allconn (void)
{
  xhash_walk (V2L_CONN_LIST, _v2l_free_walker, NULL);
  xhash_free (V2L_CONN_LIST);
}

v2l_LdapEvt *
v2l_ldap_get_entry (v2l_Config *self, v2l_LdapConn *curr_conn)
{
  v2l_LdapEvt *evt_res;
  int rc;

  evt_res = (v2l_LdapEvt *) malloc (sizeof (v2l_LdapEvt));

  if (evt_res == NULL)
    {
      LOG_ERROR_MEM;
      return NULL;
    }

  evt_res->ld = curr_conn->ld;

  rc = _v2l_ldap_search (curr_conn->entry, self->suffix, NULL, 1, evt_res);

  if (rc != LDAP_SUCCESS)
    {
      log_error (ZONE, "LDAP error attempting to retrieve user info: %s",
        ldap_err2string (rc));
      free (evt_res);
      return NULL;
    }

  _v2l_ldap_sync (evt_res);

  if (ldap_count_entries (evt_res->ld, evt_res->result) != 1)
    {
      log_warn (ZONE, "Multiple users with the same dn?");
      free (evt_res);
      return NULL;
    }

  return evt_res;
}

int
v2l_request_record (v2l_Config *self, v2l_LdapConn *curr_conn,
  v2l_LdapRequest *req)
{
  LDAPMod **attrs;
  int i, nbmod, ret;
  v2l_LdapRequest *cur_req;
  v2l_LdapEvt *evt_res;

  if (req == NULL)
    {
      log_warn (ZONE, "LDAP request is NULL? I cannot record anything");
      return 1;
    }

  nbmod = _v2l_count_attrs (req);

  attrs = (LDAPMod **) malloc ((nbmod + 1) * sizeof (LDAPMod *));

  if (attrs == NULL)
    {
      LOG_ERROR_MEM;
      return 0;
    }

  /* to wait for the results */
  evt_res = (v2l_LdapEvt *) malloc (sizeof (v2l_LdapEvt));

  if (evt_res == NULL)
    {
      LOG_ERROR_MEM;
      free (attrs);
      return 0;
    }

  for (i = 0; i < nbmod; i++)
    {
      attrs[i] = (LDAPMod *) malloc (sizeof (LDAPMod));

      if (attrs[i] == NULL)
        {
          while (--i >= 0)
            {
              free (attrs[i]);
            }

          LOG_ERROR_MEM;
          free (attrs);
          free (evt_res);
          return 0;
        }
    }

  for (cur_req = req, i = 0; i < nbmod; cur_req = cur_req->next, i++)
    {
      memcpy (attrs[i], cur_req->attr, sizeof (LDAPMod));
      log_debug (ZONE, "Element \"%s\" (%d) in the LDAP request: %s",
        attrs[i]->mod_type, i, attrs[i]->mod_values[0]);
    }

  attrs[nbmod] = NULL;

  log_debug (ZONE, "LDAP attempting to modify \"%s\" with dn \"%s\"",
  curr_conn->user, curr_conn->binddn);

  evt_res->ld = curr_conn->ld;
  evt_res->rc = _v2l_ldap_modify (curr_conn->binddn, attrs, evt_res);
  ret = evt_res->rc == LDAP_SUCCESS;

  if (!ret)
    {
      log_error (ZONE, "LDAP error attempting to modify user info: %s",
        ldap_err2string (evt_res->rc));
    }
  else
    {
      _v2l_ldap_sync (evt_res);
    }

  ldap_msgfree (evt_res->result);
  free (evt_res);

  for (cur_req = req, i = 0; i < nbmod; cur_req = cur_req->next, i++)
    {
      free (attrs[i]);
      free (cur_req->attr->mod_values[0]);
      free (cur_req->attr->mod_values);
      free (cur_req->attr);
      free (cur_req);
    }

  free (attrs);

  return ret;
}

v2l_LdapRequest *
v2l_add_attr_str (v2l_LdapRequest *req, const char *attr, const char *str)
{
  LDAPMod *mod;

  mod = (LDAPMod *) malloc (sizeof (LDAPMod));

  if (mod == NULL)
    {
      return NULL;
    }

  mod->mod_op = LDAP_MOD_REPLACE;
  mod->mod_type = (char *) attr;

  mod->mod_values = (char **) malloc (2 * sizeof (char *));

  if (mod->mod_values == NULL)
    {
      free (mod);
      return NULL;
    }

  mod->mod_values[0] = (char *) malloc (strlen (str) + 1);

  if (mod->mod_values[0] == NULL)
    {
      free (mod->mod_values);
      free (mod);
      return NULL;
    }

  memcpy (mod->mod_values[0], str, strlen (str) + 1);
  mod->mod_values[1] = NULL;

  return _v2l_add_attr (req, mod);
}

void
v2l_ldap_for_all_attrs (v2l_AttrValueFunction value_func,
  v2l_AttrMatchFunction match_func, void *pointer, v2l_LdapEvt *evt_res)
{
  LDAPMessage *current_result;
  BerElement *ber;
  char *current_attr, **vals;
  void *shrdata;

  current_result = ldap_first_entry (evt_res->ld, evt_res->result);
  current_attr = ldap_first_attribute (evt_res->ld, current_result, &ber);

  /* step through each attribute in objectclass */
  for (;
    current_attr != NULL;
    current_attr = ldap_next_attribute (evt_res->ld, current_result, ber))
    {

      if (match_func (current_attr, &shrdata))
        {
          vals = ldap_get_values (evt_res->ld, current_result, current_attr);
          value_func (current_attr, (const char **) vals, pointer, shrdata);

          if (vals != NULL)
            {
              ldap_value_free (vals);
            }
        }

      ldap_memfree (current_attr);
    } /* attributes loop */

  if (ber != NULL)
    {
      ber_free (ber, 0);
    }

  /* don't forget to free the next attribute */
  ldap_memfree (current_attr);
  ldap_msgfree (evt_res->result);
}

/* public api ends here */

static v2l_LdapConn *
_v2l_create_conn (char *host, int port, const char *binddn, const char *user,
  const char *passwd, int expire)
{
  LDAP *ld;
  v2l_LdapConn *ldap_conn;
  int version;      /* LDAP protocol version */
  int rc;
  pool poolref;

  ld = ldap_init (host, port);

  if (ld == NULL)
    {
      log_error (ZONE, "Unable to init LDAP");
      return NULL;
    }

  /* XXX */
  version = LDAP_VERSION3;
  /***/

  ldap_set_option (ld, LDAP_OPT_PROTOCOL_VERSION, &version);
  rc = ldap_simple_bind_s (ld, binddn, passwd);

  if (rc != LDAP_SUCCESS)
    {
      log_error (ZONE,
         "LDAP simple bind error : %s", ldap_err2string (rc));
      return NULL;
    }

  poolref = pool_new ();

  if (poolref == NULL)
    {
      LOG_ERROR_MEM;
      ldap_unbind_s (ld);
      return NULL;
    }

  ldap_conn = (v2l_LdapConn *) pmalloc (poolref, sizeof (v2l_LdapConn));

  if (ldap_conn == NULL)
   {
      LOG_ERROR_MEM;
      ldap_unbind_s (ld);
      pool_free (poolref);
      return NULL;
   }

  ldap_conn->poolref = poolref;
  ldap_conn->ld = ld;

  ldap_conn->binddn = (char *) pmalloc (ldap_conn->poolref,
    strlen (binddn) + 1);

  if (ldap_conn->binddn == NULL)
   {
      LOG_ERROR_MEM;
      ldap_unbind_s (ld);
      pool_free (poolref);
      return NULL;
   }

  strcpy (ldap_conn->binddn, binddn);

  ldap_conn->entry = (char *) pmalloc (ldap_conn->poolref, strlen (binddn) + 1);

  if (ldap_conn->entry == NULL)
   {
      LOG_ERROR_MEM;
      ldap_unbind_s (ld);
      pool_free (poolref);
      return NULL;
   }

  strcpy (ldap_conn->entry, binddn);
  strtok (ldap_conn->entry, ", ");

  ldap_conn->user = (char *) pmalloc (ldap_conn->poolref, strlen (user) + 1);

  if (ldap_conn->user == NULL)
   {
      LOG_ERROR_MEM;
      ldap_unbind_s (ld);
      pool_free (poolref);
      return NULL;
   }

  strcpy (ldap_conn->user, user);

  ldap_conn->creation_time = time (NULL); /* timestamp */

  if (expire == 1)   /* Add it to V2L_CONN_LIST */
    {
      _v2l_add_conn (ldap_conn);
    }

  return ldap_conn;
}

static char *
_v2l_ldap_filter (v2l_Config *self, const char *user)
{
  char *filter;

  if (self->filter == NULL || user == NULL)
    {
      log_error (ZONE, "Attempt to make a NULL filter");
      return NULL;
    }

  filter = (char *) malloc (strlen (self->filter) + strlen (user));

  if (filter == NULL)
    {
      LOG_ERROR_MEM;
      return NULL;
    }

  sprintf (filter, self->filter, user);

  return filter;
}

static int
_v2l_ldap_get_credentials (v2l_Config *self, const char *user, char **passwd,
  char **cn)
{
  LDAPMessage *e;
  v2l_LdapEvt *evt_res;
  char *filter, **vpw, **vcn;
  char *attrs[3] = {"userPassword", "cn", NULL};

  evt_res = (v2l_LdapEvt *) malloc (sizeof (v2l_LdapEvt));

  if (evt_res == NULL)
    {
      LOG_ERROR_MEM;
      return 0;
    }

  filter = _v2l_ldap_filter (self, user);

  evt_res->ld = self->master_conn->ld;
  evt_res->rc = _v2l_ldap_search (filter, self->suffix, attrs, 0, evt_res);

  free (filter);

  if (evt_res->rc != LDAP_SUCCESS)
    {
      log_error (ZONE,
        "LDAP error attempting to retrieve \"%s\"'s password: %s",
        user, ldap_err2string (evt_res->rc));
      free (evt_res);
      return 0;
    }

  _v2l_ldap_sync (evt_res);

  *passwd = NULL;
  *cn = NULL;

  if (ldap_count_entries (evt_res->ld, evt_res->result) == 1)
    {
      e = ldap_first_entry (evt_res->ld, evt_res->result);

      vpw = ldap_get_values (evt_res->ld, e, "userPassword");

      if (vpw == NULL)
        {
          log_debug (ZONE, "User has no password!");
          ldap_msgfree (evt_res->result);
          free (evt_res);
          return 0;
        }

      vcn = ldap_get_values (evt_res->ld, e, "cn");

      if (vcn == NULL)
        {
          log_debug (ZONE, "LDAP general failure!!");
          ldap_value_free (vpw);
          ldap_msgfree (evt_res->result);
          free (evt_res);
          return 0;
        }

      *passwd = (char *) malloc (strlen (vpw[0]) + 1);

      if (*passwd == NULL)
        {
          LOG_ERROR_MEM;
          ldap_value_free (vpw);
          ldap_value_free (vcn);
          ldap_msgfree (evt_res->result);
          free (evt_res);
          return 0;
        }

      strcpy (*passwd, vpw[0]);
      ldap_value_free (vpw);

      *cn = (char *) malloc (strlen (vcn[0]) + 1);

      if (*cn == NULL)
        {
          LOG_ERROR_MEM;
          free (*passwd);
          ldap_value_free (vpw);
          ldap_value_free (vcn);
          ldap_msgfree (evt_res->result);
          free (evt_res);
          return 0;
        }

      strcpy (*cn, vcn[0]);
      ldap_value_free (vcn);
    }

  ldap_msgfree (evt_res->result);
  free (evt_res);

  return *passwd != NULL && *cn != NULL;
}

static int
_v2l_ldap_modify (char *dn, LDAPMod **attrs, v2l_LdapEvt *evt_res)
{
  return ldap_modify_ext (evt_res->ld, dn, attrs, NULL, NULL,
    &(evt_res->msgid));
}

static int
_v2l_ldap_search (char *dn, char *suffix, char **attrs, int subtree,
  v2l_LdapEvt *evt_res)
{
  int scope;

  scope = subtree ? LDAP_SCOPE_SUBTREE : LDAP_SCOPE_ONELEVEL;

  return ldap_search_ext (evt_res->ld, suffix, scope,
    dn, attrs, 0, NULL, NULL, NULL, LDAP_NO_LIMIT,
    &(evt_res->msgid));
}

static void
_v2l_ldap_sync (v2l_LdapEvt *evt_res)
{
#ifndef _V2L_JABBER2
  pth_event_t evt;

  evt = pth_event (PTH_EVENT_FUNC, &_v2l_ldap_wait_callback,
       (void *) evt_res, pth_time (V2L_POLL_INTERVAL, 0));
  pth_wait (evt);
#else
  pthread_t thr;
  int rc;

  rc = pthread_create (&thr, NULL, _v2l_ldap_wait_callback_g, (void *) evt_res);

  if (rc != 0)
    {
      log_error (ZONE, "Thread create failed: %d", rc);
      return;
    }

  pthread_join (thr, NULL);
#endif
}

/*! Count the number of LDAPMod in the structure */
static int
_v2l_count_attrs (v2l_LdapRequest *req)
{
  int nbmod;

  for (nbmod = 0; req != NULL; req = req->next, nbmod++);

  return nbmod;
}

static v2l_LdapRequest *
_v2l_add_attr (v2l_LdapRequest *req, LDAPMod *attr)
{
  v2l_LdapRequest *new_req, *ptr;

  if (attr == NULL)
    {
      log_warn (ZONE, "LDAP attribute is NULL? I cannot add anything");
      return NULL;
    }

  new_req = (v2l_LdapRequest *) malloc (sizeof (v2l_LdapRequest));

  if (new_req == NULL)
    {
      LOG_ERROR_MEM;
      return NULL;
    }

  new_req->attr = attr;
  new_req->next = NULL;

  if (req == NULL)
    {
      req = new_req;
    }
  else
    {
      for (ptr = req; ptr->next != NULL; ptr = ptr->next);
      ptr->next = new_req;
    }

  return req;
}

static int
_v2l_ldap_wait_callback (void *arg)
{
    v2l_LdapEvt *evt_res = (v2l_LdapEvt *) arg;
    LDAPMessage *result;
    int rc;

    rc = ldap_result (evt_res->ld, evt_res->msgid, 1, NULL, &result);

    if (rc == -1)
      {
        log_error (ZONE, "LDAP result error %s",
            ldap_err2string (rc));
        evt_res->result = NULL;
        evt_res->rc = -1;
        return 1;
      }

    if ((rc == LDAP_RES_ADD)
      || (rc == LDAP_RES_MODIFY)
      || (rc == LDAP_RES_SEARCH_RESULT)
      || (rc == LDAP_RES_SEARCH_ENTRY)
      || (rc == LDAP_RES_DELETE))
      {
        evt_res->result = result;
        evt_res->rc = rc;
        return 1;
      }

  return 0; /* still waiting */
}

#ifdef _V2L_JABBER2
static void *
_v2l_ldap_wait_callback_g (void *arg)
{
  while (!_v2l_ldap_wait_callback (arg))
    {
      sleep (V2L_POLL_INTERVAL);
    }

  return NULL;
}
#endif

static void
_v2l_free_walker (xht h, const char *key, void *val, void *arg)
{
  _v2l_free_conn (h, key, val);
}

static void
_v2l_free_conn (xht h, const char *user, void *val)
{
  v2l_LdapConn *temp_conn = (v2l_LdapConn *) val;

  log_debug (ZONE, "Freeing LDAP connection for user \"%s\"", user);

  xhash_zap (h, user);
  ldap_unbind_s (temp_conn->ld);
  pool_free (temp_conn->poolref);
}

static void
_v2l_free_expired_walker (xht h, const char *key, void *val, void *arg)
{
  v2l_LdapConn *temp_conn = (v2l_LdapConn *) val;

  /* kill connections older than V2L_CONN_LIFETIME */
  if ((time (NULL) - temp_conn->creation_time) > (time_t) V2L_CONN_LIFETIME)
    {
      _v2l_free_conn (h, key, val);
    }
}

static void *
_v2l_purge_conn_callback (void *arg)
{
  log_debug (ZONE, "Checking connections lifetime");

  while (1)
    {
      /* V2L_CONN_LIST has been freed? */
      if (V2L_CONN_LIST != NULL)
        {
          xhash_walk (V2L_CONN_LIST, _v2l_free_expired_walker, NULL);
        }
#ifndef _V2L_JABBER2
      pth_sleep (V2L_CONN_LIFETIME);
#else
      sleep (V2L_CONN_LIFETIME);
#endif
    }

    return NULL;
}

static void
_v2l_add_conn (v2l_LdapConn *ldap_conn)
{
#ifndef _V2L_JABBER2
  pth_attr_t attr;
#else
  pthread_t thr;
  pthread_attr_t attr;
  int rc;
#endif

  if (V2L_CONN_LIST == NULL)
    {

      log_debug (ZONE, "V2L_CONN_LIST hashtable is not initialized yet");

      V2L_CONN_LIST = xhash_new (509);

      /* spawn the thread which deletes expired connections */
#ifndef _V2L_JABBER2
      attr = pth_attr_new ();
      pth_attr_set (attr, PTH_ATTR_JOINABLE, FALSE);
      pth_spawn (attr, _v2l_purge_conn_callback, NULL);
      pth_attr_destroy (attr);
#else
      pthread_attr_init (&attr);
      pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);

      rc = pthread_create (&thr, &attr, _v2l_purge_conn_callback, NULL);
      pthread_attr_destroy (&attr);

      if (rc != 0)
      {
        log_error (ZONE, "Thread create failed: %d", rc);
      }
#endif
    }

  xhash_put (V2L_CONN_LIST, ldap_conn->user, (void *) ldap_conn);
}
