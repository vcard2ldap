/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_vcard.h
    \brief XML vCard's to/from LDAP objects translation functions.
*/

#ifndef __V2L_VCARD_H
#define __V2L_VCARD_H

#include <v2l_conn.h>

/*! \brief Gets user vCard in xml format
    \param self Module config
    \param curr_conn user connection.
    \return vCard main node, NULL if error.
*/
extern xmlnode v2l_vcard_get (v2l_Config *self, v2l_LdapConn *curr_conn);

/*! \brief Writes vCard to user LDAP directory entry.
    \param self Module config
    \param curr_conn user connection.
    \param data the vCard
    \return 1 if no error, otherwise 0
*/
extern int v2l_vcard_set (v2l_Config *self, v2l_LdapConn *curr_conn,
  xmlnode data);
#endif
