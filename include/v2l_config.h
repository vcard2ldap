/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_config.h
    \brief Config handling functions.
*/

#ifndef __V2L_CONFIG_H
#define __V2L_CONFIG_H

/*! Forward declaration */
typedef struct v2l_LdapConn* v2l_LdapConnPtr;

/*! The username associated with the master connection */
#define V2L_ADMIN "jabberadmin"

/*! \brief Type of the jabberd global config.
    'xmlnode' for jabberd14, 'config_t' por jabberd2
*/
#ifdef _V2L_JABBER2
#define T_CONF config_t
#else
#define T_CONF xmlnode
#endif

/*! \brief VCard2LDAP configuration.
    Shared config for the module API.
*/
typedef struct v2l_Config
{
#ifndef _V2L_JABBER2
  xmlnode config;      /*! reference to config file (jabber14) */
#endif
  pool poolref;
  v2l_LdapConnPtr master_conn;  /*! root connection to LDAP */
  char *host;          /*!< LDAP hostname */
  int port;            /*!< LDAP port */
  char *suffix;        /*!< LDAP root dn */
  char *binddn;        /*!< dn used for "master" connections */
  char *bindpw;        /*!< pw used for "master" connections */
  char *filter;        /*!< LDAP search filter */
  char *confpath;      /*!< path to the translation template */
  int irishack;        /*!< quirks for RedIRIS schema */
} v2l_Config;

/*! \brief Reads global config and initialize module config.
    \pre jabberd config exists and it's valid.
    \post all fields of self are set.
    \param self Module config.
    \param cfgroot Global config. T_CONF type.
    \return 1 if no error, otherwise returns 0.
*/
extern int v2l_config_init (v2l_Config *self, T_CONF cfgroot);

/*! \brief Shutdown function, closes all connections and frees mempools.
    \pre self != NULL and self->master_conn is open.
    \post self->master_conn closed, all pools freeds.
    \param self Module config.
*/
extern void v2l_config_shutdown (v2l_Config *self);
#endif
