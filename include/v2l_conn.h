/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*! \file v2l_conn.h
    \brief Handling the LDAP directory. Low-level tier.
*/

#ifndef __V2L_CONN_H
#define __V2L_CONN_H

#include <ldap.h>
#include <time.h>

#include <v2l_config.h>

/*! \brief Simple Linked List of connections to LDAP directory */
typedef struct v2l_LdapConn
{
  pool poolref;
  LDAP *ld;              /*!< LDAP descriptor */
  time_t creation_time;  /*!< timestamp */
  char *binddn;          /*!< complete dn */
  char *entry;           /*!< short dn */
  char *user;            /*!< username */
  struct v2l_LdapConn *next;
} v2l_LdapConn;

/*! \brief Request (SLL) for LDAP directory */
typedef struct v2l_LdapRequest
{
  LDAPMod *attr;
  struct v2l_LdapRequest *next;
} v2l_LdapRequest;

/*! \brief LDAP environment.
    Control struct for perform operations on the LDAP directory and retrieve
    results.
*/
typedef struct v2l_LdapEvt
{
  LDAP *ld;   /*!< LDAP descriptor */
  int msgid;  /*!< LDAP message id (see OpenLDAP API docs) */
  int rc;     /*!< LDAP error code */
  LDAPMessage *result; /*!< LDAP result (see OpenLDAP API docs) */
} v2l_LdapEvt;

/*! \brief Match function type
    Generic attribute matching routine.
    \param attr The attribute.
    \param[out] shrdata Generic deferenced pointer. Shared data with 'value
    function'
    \sa v2l_AttrValueFunction
    \sa v2l_ldap_for_all_attrs
    \return true if matching, otherwise false.
*/
typedef int (*v2l_AttrMatchFunction) (const char *attr, void **shrdata);

/*! \brief Values read function type
    Generic values handling routine.
    \param attr The attribute.
    \param vals Values of attribute.
    \param pointer Generic multipurpose pointer.
    \param shrdata Shared data with 'match function'
    \sa v2l_MatchValueFunction
    \sa v2l_ldap_for_all_attrs
*/
typedef void (*v2l_AttrValueFunction) (const char *attr, const char **vals,
  void *pointer, void *shrdata);

/*! \brief Gets a (ephemeral) connection by name. Creates one if doesn't exist.
    \warning The name defined by V2L_ADMIN is reserved for master conn.
    \pre self is valid, user is not null and exists.
    \post ephermeral connection for user is open.
    \param self Module config.
    \param user the username
    \sa V2L_ADMIN
    \sa v2l_get_master_conn
    \return the connection if no error, otherwise NULL.
*/
extern v2l_LdapConn *v2l_get_conn (v2l_Config *self, const char *user);

/*! \brief Gets a connection master connection. Creates one if doesn't exist.
    \pre self is valid
    \post master connection is open.
    \param self Module config.
    \return the connection if no error, otherwise NULL.
*/
extern v2l_LdapConn *v2l_get_master_conn (v2l_Config *self);

/*! \brief Frees and closes all connections (ephemeral or not)
    If connections doesn't exist does nothing.
    \post self->master_conn closed. Connections list empty.
*/
extern void v2l_free_allconn (void);

/*! \brief Retrieves an user entry from the LDAP directory
    \note Allocated memory must be freed by the caller.
    \param self Module config
    \param curr_conn user connection.
    \return evt_res LDAP descriptor, control code and result. NULL if error.
*/
extern v2l_LdapEvt *v2l_ldap_get_entry (v2l_Config *self,
  v2l_LdapConn *curr_conn);

/*! \brief Executes a list of changes on the LDAP directory.
    \pre req is not NULL
    \param self Module config
    \param curr_conn The user connection.
    \param req list of changes.
    \return 1 if no error, otherwise 0
*/
extern int v2l_request_record (v2l_Config *self, v2l_LdapConn *curr_conn,
  v2l_LdapRequest *req);

/*! \brief Adds a new request to the list
    \param req The list of requests.
    \param attr attribute name.
    \param str attribute value.
    \return The list of requests + the last added. The list of request if error.
*/
extern v2l_LdapRequest *v2l_add_attr_str (v2l_LdapRequest *req,
  const char *attr, const char *str);

/*! \brief Applies a function to all LDAP object attributes.
    \pre evt_res should be a valid entry (ie. result of a search in directory)
    \param value_func Walker function.
    \param match_func Match function. If returns true we'll call to value_func
    \param pointer Generic multipurpose pointer. It will be passed to value_func
    \param evt_res LDAP descriptor, control code and result.
*/
extern void v2l_ldap_for_all_attrs (v2l_AttrValueFunction value_func,
  v2l_AttrMatchFunction match_func, void *pointer, v2l_LdapEvt *evt_res);
#endif
