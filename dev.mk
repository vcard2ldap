SRC = ./src
all: j143 j16
j143:
	cd $(SRC) && $(MAKE) -f Makefile.143 all
j16:
	cd $(SRC) && $(MAKE) -f Makefile.16 all
	$(MAKE) all
p16:
	@echo "Do nothing"
p214:
	-(cd ../../patch/ && diff -Naurb jabberd-2.1.14.orig jabberd-2.1.14) \
		> patch/patch-v2l-jabberd-2.1.14.diff
patches: p16 p214
commit: rclean patches
	git commit -a
dist: rclean patches
	$(RM) Makefile Makefile.in v2l-j16.xml \
		src/Makefile src/Makefile.in
	$(RM) -r src/.deps 
	-tar -czf vcard2ldap.tgz --exclude=src/.deps \
		--exclude=.git --exclude=patch/mod_*.c --exclude=vcard2ldap.* \
		--exclude=*.save --exclude doc/* \
		--exclude=dev.mk \
		../vcard2ldap
doxy:
	-doxygen 
rclean: 
	$(RM) vcard2ldap.tgz
	$(RM) -r doc/*
	cd $(SRC) && $(MAKE) -f Makefile.143 clean
	cd $(SRC) && $(MAKE) -f Makefile.16 clean
	if test -f "Makefile" ; then $(MAKE) clean ; fi
